﻿using jha.Twt.Models;
using System.Collections.Concurrent;

namespace jha.Twt.Storage
{
    public interface ITweetsStorage
    {
        ConcurrentDictionary<string, long> TweetURLStorage { get; }

        ConcurrentDictionary<string, long> PictureURLStorage { get; }

        ConcurrentDictionary<string, long> TweetHashTagStorage { get; }

        void AddTweet(Tweet tweetObject);

        void AddTweetURL(Tweet tweetObject);

        void AddTweetHashTag(Tweet tweetObject);

        long TweetCount();
    }
}