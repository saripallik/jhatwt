﻿using jha.Twt.Models;
using System.Collections.Concurrent;

namespace jha.Twt.Storage
{
    public sealed class TweetsStorage : ITweetsStorage
    {
        #region Private Variables
        private static volatile TweetsStorage instance;
        private static object syncRoot = new object();
        private ConcurrentBag<Tweet> tweetStorage;

        #endregion Private Variables

        #region Constructor
        //Private constructor
        private TweetsStorage()
        {
            tweetStorage = new ConcurrentBag<Tweet>();

            TweetURLStorage = new ConcurrentDictionary<string, long>();

            TweetHashTagStorage = new ConcurrentDictionary<string, long>();

            PictureURLStorage = new ConcurrentDictionary<string, long>();
        }
        #endregion Constructor

        #region Public Methods
        public static TweetsStorage Instance
        {
            get
            {
                if(instance == null)
                {
                    lock (syncRoot)
                    {
                        if(instance == null)
                        {
                            instance = new TweetsStorage();
                        }
                    }
                }

                return instance;
            }
        }
        #endregion Public Methods

        #region ITweetsStorage

        #region Tweets
        public void AddTweet(Tweet tweetObject)
        {
            tweetStorage.Add(tweetObject);
        }

        public long TweetCount()
        {
            return tweetStorage.Count;
        }
        #endregion Tweets

        #region #Tags

        public ConcurrentDictionary<string, long> TweetHashTagStorage { get; }

        public void AddTweetHashTag(Tweet tweetObject)
        {
            foreach (var item in tweetObject.data.entities.hashtags)
            {
                long count = 1;
                TweetHashTagStorage.AddOrUpdate(
                    item.tag, count,
                    (htag, num) =>
                    {
                        if (item.tag == htag)
                        {
                            num++;
                        }

                        return num;
                    });
            }
        }
        #endregion #Tags

        #region URLs
        public ConcurrentDictionary<string, long> TweetURLStorage { get; }
        public ConcurrentDictionary<string, long> PictureURLStorage { get; }

        public void AddTweetURL(Tweet tweetObject)
        {
            foreach (var item in tweetObject.data.entities.urls)
            {
                long count = 1;
                TweetURLStorage.AddOrUpdate(
                    item.url, count,
                    (urlID, num) =>
                    {
                        if (item.url == urlID)
                        {
                            num++;
                        }

                        return num;
                    });

                if (item.display_url.Contains("pic.twitter.com/"))
                {
                    long picCount = 1;
                    PictureURLStorage.AddOrUpdate(
                        item.display_url, picCount,
                        (urlID, num) =>
                        {
                            if (item.display_url == urlID)
                            {
                                num++;
                            }

                            return num;
                        });
                }
            }
        }
        #endregion URLs

        #endregion ITweetsStorage
    }
}