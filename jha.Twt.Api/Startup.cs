﻿using jha.Twt.Analyzer;
using jha.Twt.Analyzer.Services;
using jha.Twt.Models;
using jha.Twt.Processor;
using jha.Twt.Processor.Implementations;
using jha.Twt.Processor.Interfaces;
using jha.Twt.Storage;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace jha.Twt.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<TwtAPI>(Configuration.GetSection(nameof(TwtAPI)));

            services
                .AddMediatR(typeof(TweetStreamCommandHandler))
                .AddMediatR(typeof(TweetAnalysisQueryHandler));

            services
                 .AddSingleton<ITweetsStorage>(TweetsStorage.Instance)
                 .AddSingleton(Configuration);

            services.AddTransient<ITweetAnalysisService, TweetAnalysisService>();

            services
                .AddScoped<ITweetHandler, TweetHandler>()
                .AddScoped<ITweetProcessor, TweetProcessor>()
                .AddScoped<ITweetURLProcessor, TweetURLProcessor>()
                .AddScoped<ITweetHashTagProcessor, TweetHashTagProcessor>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddHttpClient();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();            
        }
    }
}