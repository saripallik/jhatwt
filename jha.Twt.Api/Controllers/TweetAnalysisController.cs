﻿using jha.Twt.Analyzer;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace jha.Twt.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TweetAnalysisController : ControllerBase
    {
        #region Private Variable
        private readonly IMediator mediatorObject;
        #endregion Private Variable

        #region Constructor
        public TweetAnalysisController(IMediator mediator)
        {
            mediatorObject = mediator;
        }
        #endregion Constructor

        #region Public Methods
        [HttpGet]
        public async Task<IActionResult> GetTweetAnalysis()
        {
            var query = new TweetAnalysisQuery()
            {
                GetTweetAnalysis = true
            };

            var response = await mediatorObject.Send(query);

            return new OkObjectResult(response);
        }
        #endregion Public Methods
    }
}