﻿using jha.Twt.Processor;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace jha.Twt.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessTweetsController : ControllerBase
    {
        #region Private Variable
        private readonly IMediator mediatorObject;
        #endregion Private Variable

        #region Constructor
        public ProcessTweetsController(IMediator mediator )
        {
            mediatorObject = mediator;
        }
        #endregion Constructor

        #region Public Methods
        [HttpGet]
        public async Task Start()
        {
            var command = new TweetStreamCommand() { StartConsuming = true };

            await mediatorObject.Publish(command);
        }
        #endregion Public Methods
    }
}