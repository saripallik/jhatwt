﻿using MediatR;

namespace jha.Twt.Processor
{
    public class TweetStreamCommand :INotification
    {
        public bool StartConsuming { get; set; }
    }
}