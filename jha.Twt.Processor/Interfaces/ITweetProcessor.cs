﻿using jha.Twt.Models;

namespace jha.Twt.Processor.Interfaces
{
    public interface ITweetProcessor
    {
        Tweet CreateTweet(string rawText);
    }
}