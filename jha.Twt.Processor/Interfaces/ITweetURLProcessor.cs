﻿using jha.Twt.Models;

namespace jha.Twt.Processor.Interfaces
{
    public interface ITweetURLProcessor
    {
        bool TweetHasURLs(Tweet tweet);
        void ProcessURL(Tweet tweet);
    }
}