﻿using System.Threading;
using System.Threading.Tasks;

namespace jha.Twt.Processor.Interfaces
{
    public interface ITweetHandler
    {
        Task Handle(string rawTweet, CancellationToken cancellationToken = default(CancellationToken));
    }
}