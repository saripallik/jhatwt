﻿using jha.Twt.Models;

namespace jha.Twt.Processor.Interfaces
{
    public interface ITweetHashTagProcessor
    {
        bool TweetHasHashTags(Tweet tweet);
        void ProcessHashTag(Tweet tweet);        
    }
}