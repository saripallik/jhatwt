﻿using jha.Twt.Models;
using jha.Twt.Processor.Interfaces;
using Newtonsoft.Json.Linq;
using System;

namespace jha.Twt.Processor.Implementations
{
    public class TweetProcessor : ITweetProcessor
    {
        public Tweet CreateTweet(string twtJson)
        {
            try
            {
                JObject json = JObject.Parse(twtJson);

                return json.ToObject<Tweet>();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return null;
            }            
        }
    }
}
