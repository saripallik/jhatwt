﻿using jha.Twt.Models;
using jha.Twt.Processor.Interfaces;
using jha.Twt.Storage;

namespace jha.Twt.Processor.Implementations
{
    public class TweetHashTagProcessor : ITweetHashTagProcessor
    {
        #region Private Variables
        private ITweetsStorage tweetStorage;
        #endregion Private Variables

        #region Constructor
        public TweetHashTagProcessor(ITweetsStorage storage)
        {
            tweetStorage = storage;
        }
        #endregion Constructor

        #region ITweetHashTagProcessor
        public void ProcessHashTag(Tweet tweet)
        {
            if (TweetHasHashTags(tweet))
            {
                tweetStorage.AddTweetHashTag(tweet);
            }
        }

        public bool TweetHasHashTags(Tweet tweet)
        {
            return tweet.data?.entities?.hashtags?.Count > 0;
        }
        #endregion ITweetHashTagProcessor
    }
}