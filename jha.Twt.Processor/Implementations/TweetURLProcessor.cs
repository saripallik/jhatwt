﻿using jha.Twt.Models;
using jha.Twt.Processor.Interfaces;
using jha.Twt.Storage;

namespace jha.Twt.Processor.Implementations
{
    public class TweetURLProcessor : ITweetURLProcessor
    {
        #region Private Variables
        private ITweetsStorage tweetStorage;
        #endregion Private Variables

        #region Constructor
        public TweetURLProcessor(ITweetsStorage storage)
        {
            tweetStorage = storage;
        }
        #endregion Constructor

        #region ITweetURLProcessor
        public void ProcessURL(Tweet tweet)
        {
            if(TweetHasURLs(tweet))
            {
                tweetStorage.AddTweetURL(tweet);
            }
        }

        public bool TweetHasURLs(Tweet tweet)
        {
            return tweet.data?.entities?.urls?.Count > 0;
        }        
        #endregion ITweetURLProcessor
    }
}