﻿using jha.Twt.Models;
using jha.Twt.Processor.Interfaces;
using jha.Twt.Storage;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace jha.Twt.Processor.Implementations
{
    public class TweetHandler : ITweetHandler
    {
        #region Private Variables
        private readonly ITweetProcessor tweetProcessor;

        private readonly ITweetsStorage tweetsStorage;

        private readonly ITweetURLProcessor tweetURLProcessor;
        private readonly ITweetHashTagProcessor tweetHashTagProcessor;

        DataflowLinkOptions lnkOptions;
        ExecutionDataflowBlockOptions dfOptions;

        BroadcastBlock<string> broadCastblock;

        TransformBlock<string, Tweet> tweetTransformBlock;
        BroadcastBlock<Tweet> tweetBroadCastBlock;

        ActionBlock<Tweet> tweetStorageBlock;
        ActionBlock<Tweet> tweetURLProcessorBlock;
        ActionBlock<Tweet> tweetHtagProcessorBlock;

        #endregion Private Variables

        public TweetHandler(ITweetsStorage storage, 
                                ITweetProcessor processor,
                                ITweetURLProcessor urlProcessor, 
                                ITweetHashTagProcessor hashTagProcessor)
        {
            tweetProcessor = processor;           
            tweetsStorage = storage;
            tweetURLProcessor = urlProcessor;
            tweetHashTagProcessor = hashTagProcessor;

            Initialize();
        }

        public async Task Handle(string rawTweet, CancellationToken cancellationToken)
        {
            if(cancellationToken.IsCancellationRequested)
            {
                //stop
                DoCompletion();
            }
            else
            {
                tweetTransformBlock.Post(rawTweet);
            }
        }

        private void Initialize()
        {
            lnkOptions = new DataflowLinkOptions { PropagateCompletion = true };

            dfOptions = new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 4, EnsureOrdered = false };

            tweetTransformBlock = new TransformBlock<string, Tweet>(new Func<string, Task<Tweet>>(CreateTweet));

            tweetBroadCastBlock = new BroadcastBlock<Tweet>(null, dfOptions);

            tweetStorageBlock = new ActionBlock<Tweet>(x => { StoreTweet(x); }, dfOptions);
            tweetURLProcessorBlock = new ActionBlock<Tweet>(x => { ProcessTweetURL(x); }, dfOptions);
            tweetHtagProcessorBlock = new ActionBlock<Tweet>(x => { ProcessTweetHashTag(x); }, dfOptions);

            CreatePipeline(lnkOptions);
        }

        public void CreatePipeline(DataflowLinkOptions linkOptions)
        {  

            tweetTransformBlock.LinkTo(tweetBroadCastBlock, linkOptions);

            tweetBroadCastBlock.LinkTo(tweetStorageBlock, linkOptions);
            tweetBroadCastBlock.LinkTo(tweetURLProcessorBlock, linkOptions);
            tweetBroadCastBlock.LinkTo(tweetHtagProcessorBlock, linkOptions);
        }

        private void DoCompletion()
        {
            tweetTransformBlock.Complete(); // call Complete on the first block, this will propagate down the pipeline

            tweetTransformBlock.Completion.Wait();
            tweetBroadCastBlock.Completion.Wait();

            tweetStorageBlock.Completion.Wait();
            tweetURLProcessorBlock.Completion.Wait();
            tweetHtagProcessorBlock.Completion.Wait();
        }

        private async Task<Tweet> CreateTweet(string strTweet)
        {
            return await Task.FromResult<Tweet>(tweetProcessor.CreateTweet(strTweet));
        }

        private void StoreTweet(Tweet twt)
        {
            tweetsStorage.AddTweet(twt);
        }

        private void ProcessTweetURL(Tweet twt)
        {
            tweetURLProcessor.ProcessURL(twt);
        }

        private void ProcessTweetHashTag(Tweet twt)
        {
            tweetHashTagProcessor.ProcessHashTag(twt);
        }
    }
}