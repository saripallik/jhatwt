﻿using jha.Twt.Models;
using jha.Twt.Processor.Interfaces;
using MediatR;
using Microsoft.Extensions.Options;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace jha.Twt.Processor
{
    public class TweetStreamCommandHandler : INotificationHandler<TweetStreamCommand>
    {
        #region Private Variables
        private readonly IHttpClientFactory httpClientFactory;        
        private readonly ITweetHandler tweetsHandler;

        private readonly TwtAPI apiOptions;
        #endregion Private Variable

        #region Constructor
        public TweetStreamCommandHandler(IHttpClientFactory httpFactory, 
                                            ITweetHandler tweetHandler, 
                                            IOptions<TwtAPI> options)
        {
            httpClientFactory = httpFactory;
            tweetsHandler = tweetHandler;

            apiOptions = options.Value;
        }
        #endregion Constructor

        #region Public Methods
        public async Task Handle(TweetStreamCommand notification, CancellationToken cancellationToken)
        {
            if (notification.StartConsuming)
            {
                var client = httpClientFactory.CreateClient();

                var authToken = apiOptions.APIKey;

                var twtCount = 0;

                var twt = "";
                var url = "https://api.twitter.com/2/tweets/sample/stream?tweet.fields=entities,lang";
                //var url = "https://api.twitter.com/2/tweets/sample/stream?tweet.fields=entities,lang,created_at&expansions=author_id&user.fields=created_at";
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authToken);

                using (HttpResponseMessage resp = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead))
                using (var contentStream = await resp.Content.ReadAsStreamAsync())
                {
                    using (var strReader = new StreamReader(contentStream))
                    {
                        do
                        {
                            if (!string.IsNullOrEmpty(twt))
                            {
                                await tweetsHandler.Handle(twt);
                                twtCount++;
                            }

                            if (twtCount >= apiOptions.MaxTweets)
                            {
                                await tweetsHandler.Handle("", new CancellationToken(true));
                                break;
                            }
                        } while ((twt = await strReader.ReadLineAsync()) != null);
                    };
                }
            }
        }
        #endregion Public Methods
    }
}