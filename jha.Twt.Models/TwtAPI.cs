﻿namespace jha.Twt.Models
{
    public class TwtAPI
    {
        public string APIKey { get; set; }
        public int MaxTweets { get; set; }
    }
}