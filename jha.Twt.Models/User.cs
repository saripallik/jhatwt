﻿using System;

namespace jha.Twt.Models
{
    public class User
    {
        public string username { get; set; }
        public DateTime created_at { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }
}