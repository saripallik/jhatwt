﻿using System.Collections.Generic;

namespace jha.Twt.Models
{
    public class Entities
    {
        public List<Url> urls { get; set; }
        //public List<Mention> mentions { get; set; }
        public List<Hashtag> hashtags { get; set; }
    }
}