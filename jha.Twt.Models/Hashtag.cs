﻿namespace jha.Twt.Models
{
    public class Hashtag
    {
        public int start { get; set; }
        public int end { get; set; }
        public string tag { get; set; }
    }
}