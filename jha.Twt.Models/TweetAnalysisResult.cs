﻿using System.Collections.Generic;

namespace jha.Twt.Models
{
    public class TweetAnalysisResult
    {
        public long TotalTweets { get; set; }

        public Dictionary<string, long> URLDetails { get; set; }
        public string PercentageOfTweetwHavingURLs { get; set; }
        public List<string> Top10URLs { get; set; }

        public Dictionary<string, long> PictureURLDetails { get; set; }
        public string PercentageOfTweetwHavingPictureURLs { get; set; }

        public Dictionary<string, long> HashTagDetails { get; set; }
        public string PercentageOfTweetwHavingHashTags { get; set; }
        public List<string> Top10HashTags { get; set; }
    }
}