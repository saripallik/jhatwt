﻿using System;

namespace jha.Twt.Models
{
    public class Data
    {
        public string id { get; set; }
        public string text { get; set; }
        public string lang { get; set; }        
        public DateTime created_at { get; set; }
        public Entities entities { get; set; }
    }
}