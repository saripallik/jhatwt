﻿using jha.Twt.Models;
using jha.Twt.Storage;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace jha.Twt.Analyzer.Services
{
    public class TweetAnalysisService : ITweetAnalysisService
    {
        #region Private Variables
        private readonly ITweetsStorage tweetsStorage;
        #endregion Private Variables

        #region Private Variables
        public TweetAnalysisService(ITweetsStorage storage)
        {
            tweetsStorage = storage;
        }
        #endregion Private Variables

        #region ITweetAnalysisService
        public async Task<TweetAnalysisResult> GetTweetAnalysis()
        {
            var analysisResult = new TweetAnalysisResult();

            analysisResult.TotalTweets = this.CalculateTotalTweets();

            analysisResult.URLDetails = await this.GetURLDetails();

            analysisResult.HashTagDetails = await this.GetHashTagDetails();

            analysisResult.PictureURLDetails = await this.GetPictureDetails();

            analysisResult.PercentageOfTweetwHavingURLs = "0.000 %";
            analysisResult.PercentageOfTweetwHavingHashTags = "0.000 %";
            analysisResult.PercentageOfTweetwHavingPictureURLs = "0.000 %";

            if (analysisResult.TotalTweets > 0)
            {
                analysisResult.PercentageOfTweetwHavingURLs = ((decimal)analysisResult.URLDetails.Count / (decimal)analysisResult.TotalTweets).ToString("0.### %");

                analysisResult.PercentageOfTweetwHavingHashTags = ((decimal)analysisResult.HashTagDetails.Count / (decimal)analysisResult.TotalTweets).ToString("0.### %");

                analysisResult.PercentageOfTweetwHavingPictureURLs = ((decimal)analysisResult.PictureURLDetails.Count / (decimal)analysisResult.TotalTweets).ToString("0.### %");
            }

            analysisResult.Top10URLs = this.GetTop10URLs(analysisResult.URLDetails);
            analysisResult.Top10HashTags = this.GetTop10URLs(analysisResult.HashTagDetails);

            return analysisResult;
        }
        #endregion ITweetAnalysisService

        #region Private Methods
        private long CalculateTotalTweets()
        {
            return tweetsStorage.TweetCount();
        }

        private async Task<Dictionary<string, long>> GetURLDetails()
        {
            var urlDetails = new Dictionary<string, long>();

            foreach (var item in tweetsStorage.TweetURLStorage)
            {
                urlDetails.Add(item.Key, item.Value);
            }

            return await Task.FromResult(urlDetails);
        }

        private async Task<Dictionary<string, long>> GetHashTagDetails()
        {
            var tagDetails = new Dictionary<string, long>();

            foreach (var item in tweetsStorage.TweetHashTagStorage)
            {
                tagDetails.Add(item.Key, item.Value);
            }

            return await Task.FromResult(tagDetails);
        }

        private List<string> GetTop10URLs(Dictionary<string, long> urlData)
        {
            var topURLs = urlData
                        .OrderByDescending(x => x.Value)
                        .Select(x => x.Key)
                        .Take(10).ToList();

            return topURLs;
        }

        private List<string> GetTop10HashTags(Dictionary<string, long> hTagsData)
        {
            var topHTags = hTagsData
                        .OrderByDescending(x => x.Value)
                        .Select(x => x.Key)
                        .Take(10).ToList();

            return topHTags;
        }

        private async Task<Dictionary<string, long>> GetPictureDetails()
        {
            var picDetails = new Dictionary<string, long>();

            foreach (var item in tweetsStorage.PictureURLStorage)
            {
                picDetails.Add(item.Key, item.Value);
            }

            return await Task.FromResult(picDetails);
        }

        #endregion Private Methods
    }
}