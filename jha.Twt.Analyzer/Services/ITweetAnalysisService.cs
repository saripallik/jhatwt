﻿using jha.Twt.Models;
using System.Threading.Tasks;

namespace jha.Twt.Analyzer.Services
{
    public interface ITweetAnalysisService
    {
        Task<TweetAnalysisResult> GetTweetAnalysis();
    }
}