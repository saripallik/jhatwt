﻿using jha.Twt.Analyzer.Services;
using jha.Twt.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace jha.Twt.Analyzer
{
    public class TweetAnalysisQueryHandler : IRequestHandler<TweetAnalysisQuery, TweetAnalysisResult>
    {
        #region Private Variables
        private readonly ITweetAnalysisService tweetAnalysisService;
        #endregion Private Variable

        #region Constructor
        public TweetAnalysisQueryHandler(ITweetAnalysisService service)
        {
            tweetAnalysisService = service;
        }
        #endregion Constructor

        #region IRequestHandler

        public async Task<TweetAnalysisResult> Handle(TweetAnalysisQuery request, CancellationToken cancellationToken)
        {
            if(cancellationToken.IsCancellationRequested)
            {
                return null;
            }

            if (request.GetTweetAnalysis)
            {
                return await tweetAnalysisService.GetTweetAnalysis();
            }

            return null;
        }
        #endregion IRequestHandler
    }
}