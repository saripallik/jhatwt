﻿using jha.Twt.Models;
using MediatR;

namespace jha.Twt.Analyzer
{
    public class TweetAnalysisQuery : IRequest<TweetAnalysisResult>
    {
        public bool GetTweetAnalysis { get; set; }
    }
}