﻿using jha.Twt.Processor.Implementations;
using jha.Twt.Processor.Interfaces;
using System.IO;
using Xunit;
using Moq;
using jha.Twt.Storage;

namespace jha.Twt.Tests.Twt.Processor
{
    public class URLProcessorTests
    {
        [Fact]
        public void Tweet_Has_URLs_Test()
        {
            var moqStorage = new Mock<ITweetsStorage>();

            string rawtweet = File.ReadAllText(@"Data\tweetwithURL.json");

            ITweetProcessor processor = new TweetProcessor();
            var twt = processor.CreateTweet(rawtweet);
            
            ITweetURLProcessor urlProcessor = new TweetURLProcessor(moqStorage.Object);

            var result = urlProcessor.TweetHasURLs(twt);

            Assert.True(result);
        }

        [Fact]
        public void Tweet_Has_No_URLs_Test()
        {
            var moqStorage = new Mock<ITweetsStorage>();
            string rawtweet = File.ReadAllText(@"Data\tweetwithoutURL.json");

            ITweetProcessor processor = new TweetProcessor();
            var twt = processor.CreateTweet(rawtweet);

            ITweetURLProcessor urlProcessor = new TweetURLProcessor(moqStorage.Object);

            var result = urlProcessor.TweetHasURLs(twt);

            Assert.False(result);
        }
    }
}