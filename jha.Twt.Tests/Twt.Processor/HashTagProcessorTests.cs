﻿using jha.Twt.Models;
using jha.Twt.Processor.Implementations;
using jha.Twt.Processor.Interfaces;
using jha.Twt.Storage;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace jha.Twt.Tests.Twt.Processor
{
    public class HashTagProcessorTests
    {
        [Fact]
        public void Tweet_Has_HashTags_Test()
        {
            var moqStorage = new Mock<ITweetsStorage>();

            var twt = new Tweet();
            twt.data = new Data()
            {
                id = "12345678",
                entities = new Entities()
                {
                    hashtags = new List<Hashtag>()
                       { new Hashtag(){ tag = "hastag1" },
                         new Hashtag(){ tag = "hastag2" }
                    }
                }
            };

            ITweetHashTagProcessor tagProcessor = new TweetHashTagProcessor(moqStorage.Object);

            var result = tagProcessor.TweetHasHashTags(twt);

            Assert.True(result);
        }

        [Fact]
        public void Tweet_Has_No_HashTags_Test()
        {
            var moqStorage = new Mock<ITweetsStorage>();

            var twt = new Tweet();
            twt.data = new Data()
            {
                id = "12345679"
            };

            ITweetHashTagProcessor tagProcessor = new TweetHashTagProcessor(moqStorage.Object);

            var result = tagProcessor.TweetHasHashTags(twt);

            Assert.False(result);
        }
    }
}