﻿using Xunit;
using Moq;
using jha.Twt.Storage;
using jha.Twt.Analyzer.Services;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace jha.Twt.Tests.Twt.Analyzer
{
    public class TweetAnalysisServiceTests
    {
        [Fact]
        public async Task Total_Tweet_Count_Test()
        {
            //Arrange
            var moqStorage = new Mock<ITweetsStorage>();
            moqStorage.Setup(x => x.TweetCount()).Returns(10);
            moqStorage.Setup(x => x.TweetURLStorage).Returns(new ConcurrentDictionary<string, long>());
            moqStorage.Setup(x => x.TweetHashTagStorage).Returns(new ConcurrentDictionary<string, long>());

            ITweetAnalysisService analysisService = new TweetAnalysisService(moqStorage.Object);

            //Act
            var result = await analysisService.GetTweetAnalysis();

            //Assert
            Assert.Equal(10,result.TotalTweets);
        }

        [Fact]
        public async Task Percent_Tweet_URL_Test()
        {
            //Arrange
            var moqStorage = new Mock<ITweetsStorage>();
            moqStorage.Setup(x => x.TweetCount()).Returns(10);
            moqStorage.Setup(x => x.TweetHashTagStorage).Returns(new ConcurrentDictionary<string, long>());

            var urlStorage = new ConcurrentDictionary<string, long>();
            urlStorage.TryAdd("Url1", 1);
            urlStorage.TryAdd("Url2", 5);

            moqStorage.Setup(x => x.TweetURLStorage).Returns(urlStorage);

            ITweetAnalysisService analysisService = new TweetAnalysisService(moqStorage.Object);

            //Act
            var result = await analysisService.GetTweetAnalysis();

            //Assert
            Assert.Equal("20 %", result.PercentageOfTweetwHavingURLs);
        }

        [Fact]
        public async Task Percent_Tweet_HashTag_Test()
        {
            //Arrange
            var moqStorage = new Mock<ITweetsStorage>();
            moqStorage.Setup(x => x.TweetCount()).Returns(3);
            moqStorage.Setup(x => x.TweetURLStorage).Returns(new ConcurrentDictionary<string, long>());

            var hashTagStorage = new ConcurrentDictionary<string, long>();
            hashTagStorage.TryAdd("HT1", 1);            

            moqStorage.Setup(x => x.TweetHashTagStorage).Returns(hashTagStorage);

            ITweetAnalysisService analysisService = new TweetAnalysisService(moqStorage.Object);

            //Act
            var result = await analysisService.GetTweetAnalysis();

            //Assert
            Assert.Equal("33.333 %", result.PercentageOfTweetwHavingHashTags);
        }
    }
}